package ke.co.davidwanjohi.converter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText millimeters, inches;
    Button convert,exit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        millimeters=findViewById(R.id.millimeters);
        inches=findViewById(R.id.inches);
        convert=findViewById(R.id.convert);
        exit=findViewById(R.id.exit);



        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(1);
            }
        });

        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get the entered value in the ml edit text

                String milimetres=millimeters.getText().toString();

                if (milimetres.equals("")){

                    Toast.makeText(getApplicationContext(),"Please enter a value to convert",Toast.LENGTH_SHORT).show();
                    return;
                }

                try{

                    Double value=Double.parseDouble(millimeters.getText().toString());

                    inches.setText(String.valueOf(converter(value)));
                }catch (Exception e){

                    Toast.makeText(getApplicationContext(),"Unable to covert the entered value to double",Toast.LENGTH_SHORT).show();
                }


            }
        });



    }


    public double converter(double mm){

        return mm/25.4;
    }
}
